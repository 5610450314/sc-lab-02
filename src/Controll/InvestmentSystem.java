package Controll;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import Model.BankAccount;
import View.InvestmentFrame;


/**
Controller call View and Model to manage in control's class.
View and Model aren't related each other.
Model is a BankAccount class
View is a InvestmentFrame class
Controller is a InvestmentSystem
*/

public class InvestmentSystem {
	
	/**
	set instance variable
	*/
	public InvestmentFrame invest ;
	private BankAccount account;
	private static final double INITIAL_BALANCE = 1000;
	ActionListener listener = new AddInterestListener();
	
	
	
	/**
	Process receive action from user implement  ActionListener
	when have action from user method 'actionPerformed'
	@param event 
	*/
	class AddInterestListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
        {
			//give value from user and change to double type in variable 'rate'
            double rate = Double.parseDouble(invest.getTextRate()) ;
            
            //calculate interest from 'rate' variable and call method 'getBalance' from model 
            double interest = account.getBalance() * rate / 100;
            
            /**process deposit, call method 'deposit' from model
            @param interest the interest to deposit method 
            */
            account.deposit(interest);
            
            /**process to show result to view
            call method 'setResult' from view which has @param string type
            in process sent string and append value from method 'getBalance' in model
              
             */
            invest.setResult(" "+ account.getBalance());
        }            
     }
     
	
	/**
	Controller contain main in class. 'main' is starting point when you start to run program.
	
	*/
     public static void main(String[] args) {
 		 new InvestmentSystem();
 		
 	}
	   
     /**
     Constructs a InvestmentSystem
  */
	public InvestmentSystem()
	{
		// call frame and show it and sent @param listener to the InvestmentFrame
		invest = new InvestmentFrame(listener);
		
		//in pack from view have 3 main method : 'createTextField', 'createButton','createPanel'
		invest.pack();
		invest.setVisible(true);
		invest.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBound(x,y,width,height) x,y are location of window frame when program run   
		invest.setBounds(400, 200, 450, 100);
		// call method setInvest
		setInvest();
	}
	
	
	/**
	set and create new object
	@param INITIAL_BALANCE to model 'BankAccount' 
	*/
	public void setInvest()
	{	
		account = new BankAccount(INITIAL_BALANCE);
	}
}
