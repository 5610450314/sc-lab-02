package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{
   /**
	set instance variable
   */
   private static final int FRAME_WIDTH = 450;
   private static final int FRAME_HEIGHT = 100;

   private static final double DEFAULT_RATE = 5;

   private JLabel rateLabel;
   public JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;
   
   
   
   /**
   Constructs a InvestmentFrame contain component frame
   @param ActionListener listener to the InvestmentFrame
   */
   public InvestmentFrame(ActionListener listener)
   {   
      createTextField();
      createButton(listener);
      createPanel();
      
      setSize(FRAME_WIDTH, FRAME_HEIGHT);
   }
	
   
   /**
   Method createTextField to get value form user
   and set default value = 'DEFAULT_RATE' variable.
   */
   private void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");
      resultLabel = new JLabel("balance: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   
   /**
   Method createButton to press accept form user after user filled value in TextField 
   */
   private void createButton(ActionListener list)
   {
      button = new JButton("Add Interest");
      button.addActionListener(list);
   }
   
   /**
   Method createPanel
   add component for show result 
   */
   private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   }
   
   /**
   Get value from  TextField that user type in
   @return the rateFielf 
   */
   
   public String getTextRate() 
   {
	return rateField.getText();
	}
   
   
   /**
   Set the result to show result
   @param str the string
   call 'resultLabel. to set text and sent str to show on view   
   */
   public void setResult(String str) {
	resultLabel.setText("balance: "+str);
	}
}
